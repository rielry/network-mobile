//
//  Company.swift
//  api-client
//
//  Created by Vignesh on 6/25/17.
//  Copyright © 2017 Vignesh. All rights reserved.
//

import Foundation


class Company {
	
	var id: String = ""
	var name: String = ""
	var companytype: String = ""
	var ratings: Double = 0.0
		
	

	
	
	static func parseCompanyJSONData(data: Data)->[Company]{
		var allCompanies = [Company]()
		
		do{
			let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
			
			
			//Parse JSON Data
			if let companies = jsonResult as? [Dictionary<String,AnyObject>]{
				
				for company in companies{
					
					let newCompany = Company()
					newCompany.id = company["_id"] as! String
					newCompany.name = company["name"] as! String
					newCompany.companytype = company["companytype"] as! String
					newCompany.ratings = company["ratings"] as! Double
                    allCompanies.append(newCompany)
					
				
				}
			
			}
		}catch let err{
				print(err)
			
			}
			return allCompanies
		}

	}
