//
//  DetailsVC.swift
//  api-client
//
//  Created by Vignesh on 6/26/17.
//  Copyright © 2017 Vignesh. All rights reserved.
//

import UIKit
import MapKit
import Cosmos

class DetailsVC: UIViewController {
	
	var selectedCompany: Company?
	var logInVC: LogInVC?
	
	
	@IBOutlet weak var nameLabel:UILabel!
	@IBOutlet weak var companyTypeLabel: UILabel!
    @IBOutlet weak var ratingsLabel: CosmosView!
	
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		nameLabel.text = selectedCompany?.name
		companyTypeLabel.text = selectedCompany?.companytype
		ratingsLabel.rating = selectedCompany!.ratings
		
		
    }


	
	@IBAction func backButtonTapped(sender: UIButton){
		_ = navigationController?.popViewController(animated: true)
	
	}
	
	@IBAction func reviewsButtonTapped(sender: UIButton){
        DataService.instance.getAllReviews(for: self.selectedCompany!)
        performSegue(withIdentifier : "showReviewsVC", sender : self)
        
	}
	
	@IBAction func addReviewButtonTapped(sender: UIButton){
		if AuthService.instance.isAuthenticated == true {
			performSegue(withIdentifier: "showAddReviewVC", sender: self)
		} else {
			showLogInVC()
		}
	}
	
	func showLogInVC(){
		logInVC = LogInVC()
		logInVC?.modalPresentationStyle = UIModalPresentationStyle.formSheet
		self.present(logInVC!, animated: true, completion: nil)
	}
	
		override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showReviewsSegue" {
				let destinationViewController = segue.destination as! ReviewsVC
				destinationViewController.selectedCompany = selectedCompany
			}else if segue.identifier == "showAddReviewVC" {
				let destinationViewController = segue.destination as! AddReviewVC
				destinationViewController.selectedCompany = selectedCompany

			
			}
		
	}
    

	
}
