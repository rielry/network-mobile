//
//  ReviewsVC.swift
//  api-client
//
//  Created by Vignesh on 6/27/17.
//  Copyright © 2017 Vignesh. All rights reserved.
//

import UIKit

class ReviewsVC: UIViewController {

    var dataService = DataService.instance

	var selectedCompany: Company?
	
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var tableView: UITableView!
	
    var reviews = [CompanyReview]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
		
        dataService.delegate=self
		
		tableView.delegate = self
        tableView.dataSource = self
        
		DataService.instance.delegate = self
        
		if let company = selectedCompany{
			nameLabel.text = selectedCompany?.name
			DataService.instance.getAllReviews(for:company)
		}
        
        
		tableView.rowHeight = UITableViewAutomaticDimension
		tableView.estimatedRowHeight = 140
    }

	@IBAction func backButtonTapped(sender: UIButton){
	
		_ = navigationController?.popViewController(animated : true)
		
	}


}

extension ReviewsVC: DataServiceDelegate{
	
	func companiesLoaded() {
	
	}
	
	func reviewsLoaded() {
		
        OperationQueue.main.addOperation {
            self.reviews = self.dataService.reviews
            self.tableView.reloadData()
        }

	}
}

extension ReviewsVC: UITableViewDelegate, UITableViewDataSource {

			func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
				return 150.0
			}
	
			func numberOfSections(in tableView: UITableView) -> Int {
				return 1
			}
	
			func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
				return DataService.instance.reviews.count
			}
	
			func tableView(_tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
					return UITableViewCell()
			}
	
			func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
				
					if let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as? ReviewCell{
							cell.configureCell(review: DataService.instance.reviews[indexPath.row])
							return cell
					} else {
						return UITableViewCell()
					}
			}
	

}
