//
//  AddCompanyVC.swift
//  api-client
//
//  Created by Vignesh on 6/26/17.
//  Copyright © 2017 Vignesh. All rights reserved.
//

import UIKit

class AddCompanyVC: UIViewController {

	@IBOutlet weak var nameField: UITextField!
	@IBOutlet weak var companyTypeField: UITextField!
	@IBOutlet weak var ratingsField: UITextField!
	

	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
	
	@IBAction func addButtonTapped(sender: UIButton){
		
		guard let name = nameField.text, nameField.text != ""
		
		else{
			showAlert(with: "Error", message: "Please enter a name")
			return
		}
		
		guard let companytype = companyTypeField.text, companyTypeField.text != ""
		else{
			showAlert(with: "Error", message: "Please enter a company Type")
			return
		}
		
		guard let ratings = Double(ratingsField.text!), ratingsField.text != ""
		else{
			showAlert(with: "Error", message: "Please enter a rating value")
			return
		}
		
	
		DataService.instance.addNewCompany(name: name, companytype: companytype, ratings: ratings, completion: {
			Succes in
			
			if Succes {
				print("We saved successfully ")
				
			} else {
				self.showAlert(with: "Error", message: "An error occured while saving the new company")
				print("We didn't save succesfully")
			}
		})
        
        self.dismissViewController()
	}
	
	@IBAction func cancelButtonTapped(sender: UIButton){
		dismissViewController()
	}

 
    @IBAction func backButtonTapped(sender: UIButton){
		dismissViewController()
	}
	
	func dismissViewController(){
		OperationQueue.main.addOperation {
			_ = self.navigationController?.popViewController(animated: true)
		}
	}
	
	func showAlert(with title: String?, message: String?){
		let alertController = UIAlertController(title: title,
			message: message, preferredStyle: .alert)
		let okAction = UIAlertAction(title: "OK", style: .
				default, handler: nil)
		alertController.addAction(okAction)
		present(alertController,animated: true, completion:
			nil)
	}
}
