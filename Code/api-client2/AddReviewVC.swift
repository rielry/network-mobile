//
//  AddReviewVC.swift
//  api-client
//
//  Created by Vignesh on 6/28/17.
//  Copyright © 2017 Vignesh. All rights reserved.
//

import UIKit
import Cosmos

class AddReviewVC: UIViewController {

	var selectedCompany: Company?

	@IBOutlet weak var headerLabel: UILabel!
	@IBOutlet weak var reviewTitleLabel: UITextField!
    @IBOutlet weak var reviewTextField: UITextView!
    @IBOutlet weak var ratingField: CosmosView!

    override func viewDidLoad() {
        super.viewDidLoad()

        if let company = selectedCompany {
		
			headerLabel.text! = company.name
			
		} else {
		
			headerLabel.text! = "Error"
			
		}
    }
	
	@IBAction func addButtonTapped(sender: UIButton){
	
		guard let company = selectedCompany else {
			showAlert(with: "Error", message: "Could not get selected company")
			return
		}
		
		guard let title = reviewTitleLabel.text,
			reviewTitleLabel.text != "" else {
			showAlert(with: "Error", message: "Please enter a title for your review")
			return
			}
		guard let reviewText = reviewTextField.text,
			reviewTextField.text != "" else{
			showAlert(with: "Error", message:  "Please enter some text for you review")
			return
			}

        DataService.instance.addNewReview(companyId: company.id, title: title, text: reviewText, rating:ratingField.rating, completion: { Success in
					if Success {
						print("We saved successfully")
						DataService.instance.getAllReviews(for: company)
						self.dismissViewController()
                    } else {
						self.showAlert(with: "Error", message: "An error occure saving the new Review")
						print("Save was unsuccessful!")
					}
			})
	}

    func dismissViewController(){
        OperationQueue.main.addOperation {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
	
	@IBAction func cancelButtonTapped(sender: UIButton){
       self.dismissViewController()
	}
	
	@IBAction func backButtonTapped(sender: UIButton){
       self.dismissViewController()
	}

	func showAlert(with title: String?, message: String?){
		let alertController = UIAlertController(title: title, message: message, preferredStyle:.alert)
		let okAction = UIAlertAction(title:"Error", style: .default, handler: nil)
		alertController.addAction(okAction)
		present(alertController, animated: true, completion: nil)
	}
    
}
