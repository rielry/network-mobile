//
//  LogInVC.swift
//  api-client2
//
//  Created by Work Space on 7/8/17.
//  Copyright © 2017 example.com. All rights reserved.
//

import UIKit

class LogInVC: UIViewController {
    
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var mainVC : MainVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func showMainVC(){
        mainVC = MainVC()
        mainVC?.modalPresentationStyle = UIModalPresentationStyle.formSheet
        self.present(mainVC!, animated: true, completion: nil)
    }

    @IBAction func cancelButtonTapped(sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
   	@IBAction func loginButtonTapped(sender: UIButton){
        guard let email = emailTextField.text,
            emailTextField.text != "",let pass =
            passwordTextField.text, passwordTextField.text !=
            "" else {
                self.showAlert(with: "Error", message: "Please enter an email and a password to continue")
                return
        }
        //AuthService.instance.registerUser(email: email,password: pass, completion: { Success in
        //        if Success {
                    AuthService.instance.logIn(email: email,password: pass, completion:{ Success in
                            if Success && AuthService.instance.isAuthenticated == true {
                                OperationQueue.main.addOperation {
                                    
                                        self.performSegue(withIdentifier: "showMainVC", sender: nil)
                                }
                            } else{
                                    OperationQueue.main.addOperation {
                                        self.showAlert(with: "Error",message: "Incorrect Email or Password")
                                    }
                            }
                                    })
                                                
                           // } else {
                             //   OperationQueue.main.addOperation {
                              //      self.showAlert(with: "Error",message: "An unknown Error occured saving the account")
                               // }
                                                
                                 //   }
                           // })
    }
    
    func showAlert(with title: String?, message: String?){
        let alertController = UIAlertController(title: title,
                                                message: message, preferredStyle:.alert)
        let okAction = UIAlertAction(title: "OK", style:.
            default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true,completion:
            nil)
        
    }


}
