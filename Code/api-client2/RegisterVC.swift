//
//  RegisterVC.swift
//  api-client2
//
//  Created by Work Space on 7/8/17.
//  Copyright © 2017 example.com. All rights reserved.
//

import Foundation


import UIKit

class RegisterVC: UIViewController {
    
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func cancelButtonTapped(sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
   	@IBAction func RegisterButtonTapped(sender: UIButton){
        guard let email = emailTextField.text,
            emailTextField.text != "",let pass =
            passwordTextField.text,
            passwordTextField.text != "", let confirmPass =
                confirmPasswordTextField.text , confirmPasswordTextField.text
                != "" else {
                self.showAlert(with: "Error", message: "Please enter an email and a password to continue")
                return
        }
        
        if pass == confirmPass {
       
        AuthService.instance.registerUser(email: email,password: pass, completion:{ Success in
            if Success{
                OperationQueue.main.addOperation {
                    
                    self.performSegue(withIdentifier: "showMainVC", sender: nil)
                }
            } else{
                OperationQueue.main.addOperation {
                    self.showAlert(with: "Error",message: "Please enter email and password to register!")
                }
            }
        })
        }
        else{
        
         self.showAlert(with: "Error",message: "Passwords do not match!")
        }
     
    }
    
    func showAlert(with title: String?, message: String?){
        let alertController = UIAlertController(title: title,
                                                message: message, preferredStyle:.alert)
        let okAction = UIAlertAction(title: "OK", style:.
            default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true,completion:
            nil)
        
    }
    
    
}
