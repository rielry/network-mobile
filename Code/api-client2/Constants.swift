//
//  Constants.swift
//  api-client
//
//  Created by Vignesh on 6/25/17.
//  Copyright © 2017 Vignesh. All rights reserved.
//

import Foundation

//Callbacks 
//Typealias for callbacks used in Data Service
typealias callback = (_ success:Bool)->()

//Base URL
let BASE_API_URL = "https://teamvirb.us/api/v1"

//GET all companies
let GET_ALL_COMPANY_URL = "\(BASE_API_URL)/company"

//GET all reviews for a specific company
let GET_ALL_COMPANY_REVIEWS = "\(BASE_API_URL)/company/reviews"

//POST add new Company
let POST_ADD_NEW_COMPANY = "\(BASE_API_URL)/company/add"

//POST add review for a specific company
let POST_ADD_NEW_REVIEW = "\(BASE_API_URL)/company/reviews/add"

//Boolean auth UserDefaults keys
let DEFAULTS_REGISTERED = "isRegistered"
let DEFAULTS_AUTHENTICATED = "isAuthenticated"

//Auth Email
let DEFAULTS_EMAIL = "email"

//Auth Token
let DEFAULTS_TOKEN = "authToken"

//REGISTER url
let POST_REGISTER_ACCT = "\(BASE_API_URL)/account/register"

//LOGIN url
let POST_LOGIN_ACCT = "\(BASE_API_URL)/account/login"




