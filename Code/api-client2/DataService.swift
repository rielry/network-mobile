//
//  DataService.swift
//  api-client
//
//  Created by Vignesh on 6/25/17.
//  Copyright © 2017 Vignesh. All rights reserved.


import Foundation

protocol DataServiceDelegate: class {
	
	func companiesLoaded()
	func reviewsLoaded()

}

class DataService{
	
	static let instance = DataService()
	
	weak var delegate: DataServiceDelegate?
	var companies = [Company]()
	var reviews = [CompanyReview]()
	
	//GET all company
	func getAllCompany(){
		let sessionConfig = URLSessionConfiguration.default
		
		//Create session and optionally set a url session delegate
		let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue:nil)
		
		//Create the request 
		//Get all company (GET /api/v1/company/)
		guard let URL = URL(string: GET_ALL_COMPANY_URL) else{ return }
		var request = URLRequest(url:URL)
		request.httpMethod = "GET"
		
		let task = session.dataTask(with: request, completionHandler: { (data:Data?,
				response: URLResponse?, error: Error?)-> Void in
			if(error == nil){
				//Success
				let statusCode = (response as! HTTPURLResponse).statusCode
				print("URL Session Task Succeded: HTTP \(statusCode)")
				if let data = data{
					self.companies = Company.parseCompanyJSONData(data: data)
					self.delegate?.companiesLoaded()
					
				}
			}
			else{
				//Failure
				print("URL Session Task Failed: \(error!.localizedDescription)")
			}
		
	})
	task.resume()
	session.finishTasksAndInvalidate()
  }
	
	
	//GET all reviews for a spccific Company
	func getAllReviews(for company : Company){
		let sessionConfig = URLSessionConfiguration.default
		
		let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
		
		guard let URL = URL(string: "\(GET_ALL_COMPANY_REVIEWS)/\(company.id)") else {return}
		var request = URLRequest(url: URL)
		request.httpMethod = "GET"
		
		let task = session.dataTask(with: request, completionHandler:{(data: Data?, response:
			URLResponse?, error: Error?) -> Void in
			if(error == nil){
				//Success
				let statusCode = (response as! HTTPURLResponse).statusCode
				print("URL Session Task Succeded : HTTP\(statusCode)")
				//Parse JSON data
				if let data = data {
					self.reviews = CompanyReview.parseReviewJSON(data: data)
					self.delegate?.reviewsLoaded()
					
					}
				
				}else{
					//Failure
					print("URL Session Task Failed: \(String(describing: error?.localizedDescription))")
				}
			})
			task.resume()
			session.finishTasksAndInvalidate()
		}
		
		
		//Post a new company
		func addNewCompany(name:String, companytype:String, ratings:Double, completion: @escaping callback){
			
				let json: [String: Any] = [
				
					"name": name,
					"companytype": companytype,
					"ratings": ratings
					//"geometry":[
					//
					//		"coordinates":[
					//				"lat":lattitude,
					//				"long":longitude
					//
					//			]
					//	]
				]
			
			do{
				//Serialize JSON
				let jsonData = try JSONSerialization.data(withJSONObject: json, options:.prettyPrinted)
				
					let sessionConfig = URLSessionConfiguration.default
				
					let session  = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
				
					guard let URL = URL(string: POST_ADD_NEW_COMPANY) else { return }
				
					var request = URLRequest(url:URL)
					request.httpMethod = "POST"
				
					guard let token = AuthService.instance.authToken else {
						completion(false)
						return
					}
						request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
						request.addValue("application/json", forHTTPHeaderField:"Content-Type")
				
						request.httpBody = jsonData
				
						let task = session.dataTask(with: request, completionHandler: {(data: Data?, response:
							URLResponse?, error: Error?) -> Void in
							if(error == nil){
								//Success
								//Check for status code 200 here. If it's not 200, then
								//authentication was not successful. If it is , done!
								let statusCode = (response as! HTTPURLResponse).statusCode
								print("URL Session Task Succeded: HTTP \(statusCode)")
								if statusCode != 200 {
									completion(false)
									return
								} else {
									self.getAllCompany()
								
								}
							}else{
								//Failure
								print("URL Session Task Failed: \(error?.localizedDescription)")
								completion(false)
								
							}
						})
						task.resume()
						session.finishTasksAndInvalidate()
				
				} catch let err{
					completion(false)
					print(err)
				}
			}
		
		
			//POST add a new Food Truck Review
    func addNewReview(companyId: String,title:String,text:String, rating:Double, completion: @escaping callback)
			{
				let json: [String:Any] = [
					"title":title,
					"text":text,
					"company":companyId,
					"ratings": rating
				]
				
				do {
					
                    let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
					
                    let sessionConfig = URLSessionConfiguration.default
					
					let session = URLSession(configuration: sessionConfig, delegate:nil, delegateQueue: nil)
					
					guard let URL = URL(string: "\(POST_ADD_NEW_REVIEW)/\(companyId)") else { return }
					var request = URLRequest(url:URL)
					request.httpMethod = "POST"
					
					guard let token = AuthService.instance.authToken else {
						
						completion(false)
						return
					
					}
					
					request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
					request.addValue("application/json", forHTTPHeaderField: "Content-Type")
					
					request.httpBody = jsonData
					
					
					let task = session.dataTask(with: request, completionHandler: {(data : Data?, response:
						URLResponse?,error:Error?) -> Void in
							if(error == nil){
								//Success
								let statusCode = (response as! HTTPURLResponse).statusCode
								print("URL Session Task Succeded: HTTP \(statusCode)")
								if statusCode != 200{
									completion(false)
									return
								} else {
									completion(true)
									
								}
							}else {
								//Failure
								print("URL Session Task Failed: \(error!.localizedDescription)")
								completion(false)
							}
					
					})
					task.resume()
					session.finishTasksAndInvalidate()
				
				} catch let err {
					print(err)
					completion(false)
				}
			
			}
		}
		
