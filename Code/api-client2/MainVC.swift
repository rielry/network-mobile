//
//  MainVC.swift
//  api-client2
//
//  Created by Work Space on 7/6/17.
//  Copyright © 2017 example.com. All rights reserved.
//

import UIKit

class MainVC: UIViewController {
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var addButton: UIButton!
    
    var dataService = DataService.instance
    var authService = AuthService.instance
    
    var logInVC: LogInVC?
    
    let searchController = UISearchController(searchResultsController: nil)
    
    
    var companies = [Company]()
    var filteredCompanies = [Company]()
    
    func filterContentForSearchText(searchText: String) {
        filteredCompanies = companies.filter { company in
            return company.name.lowercased().contains(searchText.lowercased())
        }
        tableView.reloadData()
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataService.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        
        DataService.instance.getAllCompany()
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
    }
    
    func showLogInVC(){
        logInVC = LogInVC()
        logInVC?.modalPresentationStyle = UIModalPresentationStyle.formSheet
        self.present(logInVC!, animated: true, completion: nil)
    }
    
    @IBAction func AddButtonTapped(sender:  UIButton){
        performSegue(withIdentifier: "showAddCompanyVC", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailsVC" {
            if let indexPath = tableView.indexPathForSelectedRow{
                let destinationViewController = segue.destination as! DetailsVC
                destinationViewController.selectedCompany = DataService.instance.companies[indexPath.row]
            }
            
        }
    }


}



extension MainVC: DataServiceDelegate {
    func companiesLoaded() {
       // print(DataService.instance.companies)
        OperationQueue.main.addOperation {
            self.companies = self.dataService.companies
            self.tableView.reloadData()
        }
    }
    func reviewsLoaded() {
       // print(DataService.instance.reviews)
    }
}

extension MainVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredCompanies.count
        }
        return companies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompanyCell", for: indexPath) as! CompanyCell
        var company: Company
        if searchController.isActive && searchController.searchBar.text != "" {
            company = filteredCompanies[indexPath.row]
        } else {
            company = companies[indexPath.row]
        }
        cell.configureCell(company: company)
        return cell
    }
}



extension MainVC: UISearchResultsUpdating {

    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchText: searchController.searchBar.text!)
    }
}
