//
//  ReviewCell.swift
//  api-client
//
//  Created by Vignesh on 6/28/17.
//  Copyright © 2017 Vignesh. All rights reserved.
//

import UIKit
import Cosmos

class ReviewCell: UITableViewCell {

	@IBOutlet weak var titlleLabel: UILabel!
	@IBOutlet weak var reviewTextLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
	
	func configureCell(review: CompanyReview){
		titlleLabel.text = review.title
		reviewTextLabel.text = review.text
	}

	
}
