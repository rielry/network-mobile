//
//  CompanyCell.swift
//  api-client
//
//  Created by Vignesh on 6/26/17.
//  Copyright © 2017 Vignesh. All rights reserved.
//

import UIKit
import Cosmos

class CompanyCell: UITableViewCell {

	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var companyTypeLabel: UILabel!
    @IBOutlet weak var ratingsLabel: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
	
	func configureCell(company: Company){
        nameLabel.text = company.name
        companyTypeLabel.text = company.companytype
        ratingsLabel.rating = company.ratings
	}
    
}
