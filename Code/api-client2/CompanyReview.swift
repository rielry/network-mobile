//
//  CompanyReview.swift
//  api-client
//
//  Created by Vignesh on 6/25/17.
//  Copyright © 2017 Vignesh. All rights reserved.
//

import Foundation

struct CompanyReview{
	
	var id: String = ""
	var title: String = ""
	var text: String = ""
    var ratings: Double = 0.0
	
	
	static func parseReviewJSON(data: Data) -> [CompanyReview]{
		var companyReviews = [CompanyReview]()
		
		do{
			let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
			//Parse JSON data
			if let reviews = jsonResult as? [Dictionary<String,AnyObject>]{
				for review in reviews{
				
				var newReview = CompanyReview()
				newReview.id = review["_id"] as! String
				newReview.title = review["title"] as! String
				newReview.text = review["text"] as! String
                //newReview.ratings = (review["ratings"] as? Double)!
                companyReviews.append(newReview)
				
				}
			
			}
		}catch let err{
			print(err)
		}
		return companyReviews
	}

}
